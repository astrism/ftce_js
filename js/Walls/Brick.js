// $origin:Point
// $wallConfig:WallConfig
// $wallStyle:WallStyle
Rock = function($origin, $wallConfig, $wallStyle)
{
	var wall = new Wall($origin, $wallConfig, $wallStyle);
	
	// initial position of x/y
	wall.xPoints = [getBlockWidth()];
	wall.yPoints = [wall.yOffset * -1];
	
	wall.generatePoints = function(xOffset, yOffset) {
		var newY = 0;
		var points = wall.offsetCurrentPoints(xOffset, yOffset);
		var yPoints = points.yPoints;
		var xPoints = points.xPoints;
		
		prevY = wall.yPoints[wall.yPoints.length - 1];
		
		if(prevY < wall.wallConfig.wallSize.y)
		{
			while(prevY < wall.wallConfig.wallSize.y)
			{
				prevY += getBlockHeight();
				yPoints.push(prevY);
				xPoints.push(getBlockWidth());
			}
		}		
		
		wall.xPoints = xPoints;
		wall.yPoints = yPoints;
	}

	function getBlockWidth()
	{
		return Math.floor(wall.wallConfig.blockJitter.x + Math.random()*(wall.wallConfig.blockSize.x - wall.wallConfig.blockJitter.x));
	}

	function getBlockHeight()
	{
		return Math.floor(wall.wallConfig.blockJitter.y + Math.random()*(wall.wallConfig.blockSize.y - wall.wallConfig.blockJitter.y));
	}
	
	return wall;
}