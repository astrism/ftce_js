var FTCE;

$(document).ready(function () {	
	var d = $(document);
    FTCE = new FTCE($('body'), d.width(), d.height());

	var onEachFrame;
	if (window.webkitRequestAnimationFrame) {
		onEachFrame = function(cb) {
			var _cb = function() { cb(); webkitRequestAnimationFrame(_cb); }
			_cb();
		};
	} else if (window.mozRequestAnimationFrame) {
		onEachFrame = function(cb) {
			var _cb = function() { cb(); mozRequestAnimationFrame(_cb); }
			_cb();
		};
	} else {
		onEachFrame = function(cb) {
			setInterval(cb, 10);
		}
	}

	window.onEachFrame = onEachFrame;

	window.onEachFrame(FTCE.run);

});